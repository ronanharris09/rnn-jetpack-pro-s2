# RNN Jetpack Pro S2
This is an Android project built with Kotlin, Fuel, Kodein, and GSON. It uses TMDB API in accordance to Dicoding's Android Jetpack Pro Course 2nd submission requirements.

# License
Please have a look at `LICENSE`

# Links
	- [Official Repo at Gitlab](https://gitlab.com/ronanharris09/rnn-jetpack-pro-s2)

