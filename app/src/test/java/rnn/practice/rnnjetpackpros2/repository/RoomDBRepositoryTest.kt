package rnn.practice.rnnjetpackpros2.repository

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.asLiveData
import androidx.paging.*
import androidx.room.RoomDatabase
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.TestCoroutineScope
import kotlinx.coroutines.test.runBlockingTest
import org.hamcrest.core.IsEqual
import org.junit.After
import org.junit.Before
import org.junit.Assert.*
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito.*
import org.mockito.junit.MockitoJUnitRunner
import org.mockito.kotlin.stub
import rnn.practice.rnnjetpackpros2.application.AppDefinition
import rnn.practice.rnnjetpackpros2.database.FavoriteTMDBShowDB
import rnn.practice.rnnjetpackpros2.models.roomdb.FavoriteTMDBShow
import rnn.practice.rnnjetpackpros2.viewmodels.TMDBViewModel
import kotlin.time.Duration
import kotlin.time.ExperimentalTime

@RunWith(MockitoJUnitRunner::class)
class RoomDBRepositoryTest {

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    private val testCoroutineDispatcher = TestCoroutineDispatcher()
    private val testCoroutineScope = TestCoroutineScope(testCoroutineDispatcher)

    private var checkCounter = 0

    private lateinit var repository: RoomDBRepository
    private lateinit var database: FavoriteTMDBShowDB

    @Before
    fun setUp() {
        database = FavoriteTMDBShowDB.initDatabase(AppDefinition())
        repository = RoomDBRepository(AppDefinition(), database)
    }

    @After
    fun tearDown() {
        testCoroutineDispatcher.cancel()
        println("# $checkCounter Checks Passed\n")
    }

    @Test
    fun `simulate an activity of adding a favorite item for both movie and tv show`() = runBlockingTest {
        repository = mock(RoomDBRepository::class.java)

        launch(testCoroutineDispatcher) {

            println("# Currently Asserting -> simulate an activity of adding favorite")

            // Mock addFavorite() method to execute a query to FavoriteTMDBShowDB to add a movie to table_favorites and then return a Boolean value as a mock return data
            `when`(repository.addFavorite(FavoriteTMDBShow(intId = 360319, stringTitle = "Case Closed", stringDate = "1988-04-19", stringPoster = null, stringOverview = null, isMovie = true))).thenReturn(true)

            println("> [movie] addFavorite() query execution check")
            // Asserts whether addFavorite() is executed or not
            assertEquals(true, repository.addFavorite(FavoriteTMDBShow(intId = 360319, stringTitle = "Case Closed", stringDate = "1988-04-19", stringPoster = null, stringOverview = null, isMovie = true)))
            checkCounter++

            // Mock addFavorite() method to execute a query to FavoriteTMDBShowDB to add a tv show to table_favorites and then return a Boolean value as a mock return data
            `when`(repository.addFavorite(FavoriteTMDBShow(intId = 30983,stringTitle = "Case Closed", stringDate = "1996-01-08",  stringPoster = null, stringOverview = null, isMovie = false))).thenReturn(true)

            println("> [tv show] addFavorite() query execution check")
            // Asserts whether addFavorite() is executed or not
            assertEquals(true, repository.addFavorite(FavoriteTMDBShow(intId = 30983,stringTitle = "Case Closed", stringDate = "1996-01-08",  stringPoster = null, stringOverview = null, isMovie = false)))
            checkCounter++

        }

        testCoroutineDispatcher.cancel()
    }

    @Test
    fun `simulate an activity of finding a favorite item for both movie and tv show`() = runBlockingTest {
        repository = mock(RoomDBRepository::class.java)

        launch(testCoroutineDispatcher) {

            println("# Currently Asserting -> simulate an activity of finding a favorite item for both movie and tv show")

            // Mock findFavorite() to execute a query to RoomDBRepository to find a specific row with the related intId from table_favorites and then return a FavoriteTMDBShow value as a mock return data
            `when`(repository.findFavorite(360319)).thenReturn(FavoriteTMDBShow(intId = 360319, stringTitle = "Case Closed", stringDate = "1988-04-19", stringPoster = null, stringOverview = null, isMovie = true))

            // Asserts whether findFavorite() is executed or not
            assertEquals(FavoriteTMDBShow(intId = 360319, stringTitle = "Case Closed", stringDate = "1988-04-19", stringPoster = null, stringOverview = null, isMovie = true), repository.findFavorite(360319))
            println("> [movie] findFavorite() query execution check")
            checkCounter++

            // Mock findFavorite() to execute a query to RoomDBRepository to find a specific row with the related intId from table_favorites and then return a FavoriteTMDBShow value as a mock return data
            `when`(repository.findFavorite(30983)).thenReturn(FavoriteTMDBShow(intId = 30983,stringTitle = "Case Closed", stringDate = "1996-01-08",  stringPoster = null, stringOverview = null, isMovie = false))

            println("> [tv show] findFavorite() query execution check")
            // Asserts whether findFavorite() is executed or not
            assertEquals(FavoriteTMDBShow(intId = 30983,stringTitle = "Case Closed", stringDate = "1996-01-08",  stringPoster = null, stringOverview = null, isMovie = false), repository.findFavorite(30983))
            checkCounter++

        }

        testCoroutineDispatcher.cancel()
    }

    @Test
    fun `simulate an activity of removing a favorite item for both movie and tv show`() = runBlockingTest {
        repository = mock(RoomDBRepository::class.java)

        launch(testCoroutineDispatcher) {

            println("# Currently Asserting -> simulate an activity of removing a favorite item for both movie and tv show")

            // Mock deleteFavorite() method to execute a query to FavoriteTMDBShowDB to remove a movie to table_favorites and then return a Boolean value as a mock return data
            `when`(repository.deleteFavorite(FavoriteTMDBShow(intId = 360319, stringTitle = "Case Closed", stringDate = "1988-04-19", stringPoster = null, stringOverview = null, isMovie = true))).thenReturn(true)

            println("> [movie] deleteFavorite() query execution check")
            // Asserts whether deleteFavorite() is executed or not
            assertEquals(true, repository.deleteFavorite(FavoriteTMDBShow(intId = 360319, stringTitle = "Case Closed", stringDate = "1988-04-19", stringPoster = null, stringOverview = null, isMovie = true)))
            checkCounter++

            // Mock deleteFavorite() method to execute a query to FavoriteTMDBShowDB to remove a tv show to table_favorites and then return a Boolean value as a mock return data
            `when`(repository.deleteFavorite(FavoriteTMDBShow(intId = 30983,stringTitle = "Case Closed", stringDate = "1996-01-08",  stringPoster = null, stringOverview = null, isMovie = false))).thenReturn(true)

            println("> [tv show] deleteFavorite() query execution check")
            // Asserts whether deleteFavorite() is executed or not
            assertEquals(true, repository.deleteFavorite(FavoriteTMDBShow(intId = 30983,stringTitle = "Case Closed", stringDate = "1996-01-08",  stringPoster = null, stringOverview = null, isMovie = false)))
            checkCounter++

        }

        testCoroutineDispatcher.cancel()
    }

    @Test
    fun `simulate an activity of resetting table_favorites`() = runBlockingTest {
        repository = mock(RoomDBRepository::class.java)

        launch(testCoroutineDispatcher) {

            println("# Currently Asserting -> simulate an activity of resetting table_favorites")

            // Mock resetFavorite() to execute a query to FavoriteTMDBShowDB to reset table_favorites and then return a Boolean value as a mock return data
            `when`(repository.resetFavorite()).thenReturn(true)

            // Asserts whether resetFavorite() is executed or not
            println("> resetFavorite() execution check")
            assertEquals(true, repository.resetFavorite())
            checkCounter++

        }

        testCoroutineDispatcher.cancel()
    }

    @ExperimentalStdlibApi
    @ExperimentalPagingApi
    @Test
    fun `simulate an activity of paging data for favorites`() = runBlockingTest {

        launch(testCoroutineDispatcher) {
            println("# Currently Asserting -> simulate an activity of paging data")

            // Mock favoritePagingFlow() through favoritePagingFlowFake() as a fake implementation to simulate a stream data of FavoriteTMDBShow as PagingData and then return a List of FavoriteTMDBShows PagingData value as a mock return data
            lenient().`when`(repository.favoritePagingFlowFake()).thenReturn(true)

            // Asserts whether favoritePagingFlowFake() is executed or not
            println("> favoritePagingFlow() result check")
            assertEquals(true, repository.favoritePagingFlowFake())
            checkCounter++

        }

        testCoroutineDispatcher.cancel()
    }
}