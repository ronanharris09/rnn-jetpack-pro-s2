package rnn.practice.rnnjetpackpros2.viewmodels

import android.util.Log
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.paging.ExperimentalPagingApi
import androidx.paging.PagingData
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.cancel
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.launch
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.runBlockingTest
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mockito.`when`
import org.mockito.Mockito.mock
import org.mockito.junit.MockitoJUnitRunner
import rnn.practice.rnnjetpackpros2.application.AppDefinition
import rnn.practice.rnnjetpackpros2.models.roomdb.FavoriteTMDBShow
import rnn.practice.rnnjetpackpros2.models.tmdb.genres.ShowGenre
import rnn.practice.rnnjetpackpros2.models.tmdb.movie.MovieDetails
import rnn.practice.rnnjetpackpros2.models.tmdb.movie.releasedates.MovieReleaseDate
import rnn.practice.rnnjetpackpros2.models.tmdb.movie.releasedates.MovieReleaseDatesResults
import rnn.practice.rnnjetpackpros2.models.tmdb.movie.releasedates.certifications.MovieCertification
import rnn.practice.rnnjetpackpros2.models.tmdb.search.movie.SearchMovie
import rnn.practice.rnnjetpackpros2.models.tmdb.search.movie.SearchMovieItem
import rnn.practice.rnnjetpackpros2.models.tmdb.search.tvshows.SearchTVShows
import rnn.practice.rnnjetpackpros2.models.tmdb.search.tvshows.SearchTVShowsItem
import rnn.practice.rnnjetpackpros2.models.tmdb.tvshows.TVShowsDetails
import rnn.practice.rnnjetpackpros2.models.tmdb.tvshows.contentratings.TVShowsContentRating
import rnn.practice.rnnjetpackpros2.models.tmdb.tvshows.contentratings.TVShowsContentRatingsResult
import rnn.practice.rnnjetpackpros2.utils.RNNState

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class TMDBViewModelTest {

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    private val testCoroutineDispatcher = TestCoroutineDispatcher()

    private var checkCounter = 0

    private lateinit var viewModel: TMDBViewModel

    @Before
    fun setUp() {
        viewModel = TMDBViewModel(AppDefinition())
    }


    @After
    fun tearDown() {
        testCoroutineDispatcher.cancel()
        println("# $checkCounter Checks Passed\n")
    }

    @Test
    fun `simulate a single movie details query`() = runBlockingTest {

        viewModel = mock(TMDBViewModel::class.java)

        launch(testCoroutineDispatcher) {

            println("# Currently Asserting -> simulate a single movie details query")

            // Mock queryMovie() method to query a detailed info of a specific movie id and the mock the return data to simulate a situation where the job is executed
            `when`(viewModel.queryMovie("32022")).thenReturn(true)

            println("> queryMovie() Job execution check")
            // Asserts whether queryMovie() is executed or not
            assertEquals(true, viewModel.queryMovie("32022"))
            checkCounter++

            // Mock getMovie() method to get the current movie details data by calling dummyMovieDetails() function to return the mock data
            `when`(viewModel.getMovie()).thenReturn(dummyMovieDetails())

            println("> getMovie() Movie ID Check")
            // Asserts whether the id of the movie is equal to 32022
            assertEquals(32022, viewModel.getMovie().value?.intId)
            checkCounter++

            println("> getMovie() Movie Title Check")
            // Asserts whether the title of the movie is equal to "Case Closed: Countdown to Heaven"
            assertEquals("Case Closed: Countdown to Heaven", viewModel.getMovie().value?.stringTitle)
            checkCounter++
        }

        testCoroutineDispatcher.cancel()
    }

    @Test
    fun `simulate a single tv shows details query`() = runBlockingTest {

        viewModel = mock(TMDBViewModel::class.java)

        launch(testCoroutineDispatcher) {

            println("# Currently Asserting -> simulate a single tv shows details query")

            // Mock queryTVShow() method to query a detailed info of a specific tv show id and the mock the return data to simulate a situation where the job is executed
            `when`(viewModel.queryTVShow("30983")).thenReturn(true)

            println("> queryTVShow() Job execution check")
            // Asserts whether queryTVShow() is executed or not
            assertEquals(true, viewModel.queryTVShow("30983"))
            checkCounter++

            // Mock getTVShow() method to get the current tv show details data by calling dummyTVShowDetails() function to return the mock data
            `when`(viewModel.getTVShow()).thenReturn(dummyTVShowDetails())

            println("> getTVShow() Movie ID Check")
            // Asserts whether the id of the movie is equal to 32022
            assertEquals(30983, viewModel.getTVShow().value?.intId)
            checkCounter++

            println("> getTVShow() Movie Title Check")
            // Asserts whether the title of the movie is equal to "Case Closed"
            assertEquals("Case Closed", viewModel.getTVShow().value?.stringTitle)
            checkCounter++
        }

        testCoroutineDispatcher.cancel()
    }

    @Test
    fun `simulate a movie search items retrieval'`() = runBlockingTest {
        viewModel = mock(TMDBViewModel::class.java)

        launch(testCoroutineDispatcher) {

            println("# Currently Asserting -> simulate a movie search items retrieval")

            // Mock queryMovies() to execute a query to TMDB API asynchronously and then return a Boolean value as a mock return data
             `when`(viewModel.queryMovies("Case Closed")).thenReturn(true)

            // Asserts whether queryMovies() is executed or not
             assertEquals(true, viewModel.queryMovies("Case Closed"))
             checkCounter++

            // Mock getMovies() method to get a list of search results by calling dummySearchMovies() function to return the mock data
            `when`(viewModel.getMovies()).thenReturn(dummySearchMovies())

            println("> getMovies() Item count check")
            // Asserts whether the total items of the search result is equal to three
            assertEquals(3, viewModel.getMovies().value?.intTotal_results)
            checkCounter++

            println("> getMovies() Page count check")
            // Asserts whether the total pages of the search result is equal to one
            assertEquals(1, viewModel.getMovies().value?.intTotal_pages)
            checkCounter++

            println("> getMovies() Page index check")
            // Asserts whether the current page index of the search result is equal to zero
            assertEquals(0, viewModel.getMovies().value?.intPage)
            checkCounter++

            println("> getMovies() Array size check")
            // Asserts whether the total size of the array of the search result items is equal to three
            assertEquals(3, viewModel.getMovies().value?.arrayResults?.size)
            checkCounter++

            println("> getMovies() Movie ID check")
            // Asserts whether the id of the movie at the index of 2 is equal to 32022
            assertEquals(32022, viewModel.getMovies().value?.arrayResults?.get(2)?.intId)
            checkCounter++

            println("> getMovies() Movie title check")
            // Asserts whether the title of the movie at the index of 2 is equal to "Case Closed: Countdown to Heaven"
            assertEquals("Case Closed: Countdown to Heaven", viewModel.getMovies().value?.arrayResults?.get(2)?.stringTitle)
            checkCounter++
        }

        testCoroutineDispatcher.cancel()
    }

    @Test
    fun `simulate a tv show search items retrieval'`() = runBlockingTest {
        viewModel = mock(TMDBViewModel::class.java)

        launch(testCoroutineDispatcher) {

            println("# Currently Asserting -> simulate a tv show search items retrieval")

            // Mock queryTVShows() to execute a query to TMDB API asynchronously and then return a Boolean value as a mock return data
             `when`(viewModel.queryTVShows("Case Closed")).thenReturn(true)

            // Asserts whether queryTVShows() is executed or not
             assertEquals(true, viewModel.queryTVShows("Case Closed"))
             checkCounter++

            // Mock getTVShows() method to get a list of search results by calling dummySearchTVShows() function to return the mock data
            `when`(viewModel.getTVShows()).thenReturn(dummySearchTVShows())

            println("> getTVShows() Item count check")
            // Asserts whether the total items of the search result is equal to three
            assertEquals(3, viewModel.getTVShows().value?.intTotal_results)
            checkCounter++

            println("> getTVShows() Page count check")
            // Asserts whether the total pages of the search result is equal to one
            assertEquals(1, viewModel.getTVShows().value?.intTotal_pages)
            checkCounter++

            println("> getTVShows() Page index check")
            // Asserts whether the current page index of the search result is equal to zero
            assertEquals(0, viewModel.getTVShows().value?.intPage)
            checkCounter++

            println("> getTVShows() Array size check")
            // Asserts whether the total size of the array of the search result items is equal to three
            assertEquals(3, viewModel.getTVShows().value?.arrayResults?.size)
            checkCounter++

            println("> getTVShows() TV Show ID check")
            // Asserts whether the id of the tv show at the index of 1 is equal to 30983
            assertEquals(30983, viewModel.getTVShows().value?.arrayResults?.get(1)?.intId)
            checkCounter++

            println("> getTVShows() TV Show title check")
            // Asserts whether the title of the movie at the index of 1 is equal to "Case Closed"
            assertEquals("Case Closed", viewModel.getTVShows().value?.arrayResults?.get(1)?.stringTitle)
            checkCounter++
        }

        testCoroutineDispatcher.cancel()
    }

    @Test
    fun `simulate a database related activities related to favorites feature for both movies and tv shows'`() = runBlockingTest {
        viewModel = mock(TMDBViewModel::class.java)

        launch(testCoroutineDispatcher) {

            println("# Currently Asserting -> simulate a database related activities related to favorites feature for both movies and tv shows")

            // Mock addFavorite() to execute a query to RoomDBRepository to add a movie to table_favorites and then return a Boolean value as a mock return data
            `when`(viewModel.addFavorite(FavoriteTMDBShow(intId = 360319, stringTitle = "Case Closed", stringDate = "1988-04-19", stringPoster = null, stringOverview = null, isMovie = true))).thenReturn(true)

            // Asserts whether addFavorite() for movie is executed or not
            println("> [movie] addFavorite() execution check")
            assertEquals(true, viewModel.addFavorite(FavoriteTMDBShow(intId = 360319, stringTitle = "Case Closed", stringDate = "1988-04-19", stringPoster = null, stringOverview = null, isMovie = true)))
            checkCounter++

            // Mock findFavorite() to execute a query to RoomDBRepository to find a specific row with the related intId from table_favorites and then return a Boolean value as a mock return data
            `when`(viewModel.findFavorite(360319)).thenReturn(true)

            // Asserts whether findFavorite() for movie is executed or not
            println("> [movie] findFavorite() execution check")
            assertEquals(true, viewModel.findFavorite(360319))
            checkCounter++

            // Mock removeFavorite() to execute a query to RoomDBRepository to remove a movie from table_favorites and then return a Boolean value as a mock return data
            `when`(viewModel.removeFavorite(FavoriteTMDBShow(keyIndex = 1, intId = 360319, stringTitle = "Case Closed", stringDate = "1988-04-19", stringPoster = null, stringOverview = null, isMovie = true))).thenReturn(true)

            // Asserts whether removeFavorite() for movie is executed or not
            println("> [movie] removeFavorite() execution check")
            assertEquals(true, viewModel.removeFavorite(FavoriteTMDBShow(keyIndex = 1, intId = 360319, stringTitle = "Case Closed", stringDate = "1988-04-19", stringPoster = null, stringOverview = null, isMovie = true)))
            checkCounter++

            // Mock addFavorite() to execute a query to RoomDBRepository to add a tv show to table_favorites and then return a Boolean value as a mock return data
            `when`(viewModel.addFavorite(FavoriteTMDBShow(intId = 30983,stringTitle = "Case Closed", stringDate = "1996-01-08",  stringPoster = null, stringOverview = null, isMovie = false))).thenReturn(true)

            // Asserts whether addFavorite() for tv show is executed or not
            println("> [tv show] addFavorite() execution check")
            assertEquals(true, viewModel.addFavorite(FavoriteTMDBShow(intId = 30983,stringTitle = "Case Closed", stringDate = "1996-01-08",  stringPoster = null, stringOverview = null, isMovie = false)))
            checkCounter++

            // Mock findFavorite() to execute a query to RoomDBRepository to find a specific row with the related intId from table_favorites and then return a Boolean value as a mock return data
            `when`(viewModel.findFavorite(30983)).thenReturn(true)

            // Asserts whether findFavorite() for movie is executed or not
            println("> [tv show] findFavorite() execution check")
            assertEquals(true, viewModel.findFavorite(30983))
            checkCounter++

            // Mock removeFavorite() to execute a query to RoomDBRepository to remove a tv show from table_favorites and then return a Boolean value as a mock return data
            `when`(viewModel.removeFavorite(FavoriteTMDBShow(keyIndex = 1, intId = 30983,stringTitle = "Case Closed", stringDate = "1996-01-08",  stringPoster = null, stringOverview = null, isMovie = false))).thenReturn(true)

            // Asserts whether removeFavorite() for tv show is executed or not
            println("> [tv show] removeFavorite() execution check")
            assertEquals(true, viewModel.removeFavorite(FavoriteTMDBShow(keyIndex = 1, intId = 30983,stringTitle = "Case Closed", stringDate = "1996-01-08",  stringPoster = null, stringOverview = null, isMovie = false)))
            checkCounter++

            // Mock resetFavorite() to execute a query to RoomDBRepository to reset table_favorites and then return a Boolean value as a mock return data
            `when`(viewModel.resetFavorite()).thenReturn(true)

            // Asserts whether resetFavorite() is executed or not
            println("> resetFavorite() execution check")
            assertEquals(true, viewModel.resetFavorite())
            checkCounter++
        }

        testCoroutineDispatcher.cancel()
    }

    @ExperimentalPagingApi
    @Test
    fun `simulate a paging activity for favorites'`() = runBlockingTest {
        viewModel = mock(TMDBViewModel::class.java)

        launch(testCoroutineDispatcher) {

            println("# Currently Asserting -> simulate a paging activity for favorites")

            // Mock pagingFavorite() to stream data of FavoriteTMDBShow as PagingData and then return a List of FavoriteTMDBShows PagingData value as a mock return data
            `when`(viewModel.pagingFavorite()).thenReturn(flowOf(PagingData.from(dummyFavoriteItems())))

            // Asserts whether pagingFavorite() for tv show is executed or not
            println("> pagingFavorite() result check")
            assertEquals(flowOf(PagingData.from(dummyFavoriteItems())).collectLatest{ }, viewModel.pagingFavorite().collectLatest {  })
            checkCounter++
        }

        testCoroutineDispatcher.cancel()
    }

    private fun dummyMovieDetails() : LiveData<MovieDetails> {
        return MutableLiveData<MovieDetails>().apply {
            this.postValue(MovieDetails(32022, "Case Closed: Countdown to Heaven","2001-04-21", null, null, arrayListOf(ShowGenre(null)), MovieReleaseDatesResults(arrayListOf(MovieReleaseDate(null, arrayListOf(MovieCertification(null))))), 0))
        }
    }

    private fun dummyTVShowDetails() : LiveData<TVShowsDetails> {
        return MutableLiveData<TVShowsDetails>().apply {
            this.postValue(TVShowsDetails(30983, "Case Closed", "1996-01-08", null, null, arrayListOf(ShowGenre(null)), TVShowsContentRatingsResult(arrayListOf(TVShowsContentRating(null))), arrayListOf(0), 0))
        }
    }

    private fun dummySearchMovies() : LiveData<SearchMovie> {
        return MutableLiveData<SearchMovie>().apply {
            this.postValue(SearchMovie(
                arrayListOf(
                    SearchMovieItem(360319,"Case Closed", "1988-04-19", null, null),
                    SearchMovieItem(631004,"Case Closed", "2017-12-31", null, null),
                    SearchMovieItem(32022,"Case Closed: Countdown to Heaven", "2001-04-21", null, null)
                ), 0, 1, 3
            ))
        }
    }

    private fun dummySearchTVShows() : LiveData<SearchTVShows> {
        return MutableLiveData<SearchTVShows>().apply {
            this.postValue(SearchTVShows(
                arrayListOf(
                    SearchTVShowsItem(67076,"D.B. Cooper: Case Closed?", "2016-07-10", null,null),
                    SearchTVShowsItem(30983,"Case Closed", "1996-01-08", null,null),
                    SearchTVShowsItem(75114,"The Hunt for the Zodiac Killer", "2017-11-14", null,null)
                ), 0,1,3
            ))
        }
    }

    private fun dummyListener(state: RNNState) = runBlockingTest {
        Log.i("RNNUnitTest", "state: $state")
    }

    private fun dummyFavoriteItems() : List<FavoriteTMDBShow> {
        return listOf(
            FavoriteTMDBShow(keyIndex = 1, intId = 360319, stringTitle = "Case Closed", stringDate = "1988-04-19", stringPoster = null, stringOverview = null, isMovie = true),
            FavoriteTMDBShow(keyIndex = 2, intId = 30983,stringTitle = "Case Closed", stringDate = "1996-01-08",  stringPoster = null, stringOverview = null, isMovie = false)
        )
    }
}