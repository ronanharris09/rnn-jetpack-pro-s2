package rnn.practice.rnnjetpackpros2.ui

import android.view.KeyEvent
import android.widget.EditText
import android.widget.SearchView
import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.IdlingRegistry
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.rules.activityScenarioRule
import com.google.android.material.tabs.TabLayout
import kotlinx.coroutines.delay
import org.hamcrest.CoreMatchers.allOf
import org.hamcrest.CoreMatchers.not
import org.junit.After
import org.junit.Before

import org.junit.Assert.*
import org.junit.Rule
import org.junit.Test
import rnn.practice.rnnjetpackpros2.R
import rnn.practice.rnnjetpackpros2.adapters.SearchTVShowsResultAdapter
import rnn.practice.rnnjetpackpros2.application.AppDefinition
import rnn.practice.rnnjetpackpros2.utils.IdlingResourceInstance
import rnn.practice.rnnjetpackpros2.viewmodels.TMDBViewModel

class MainActivityTest {

    private var viewModel: TMDBViewModel? = null

    @Before
    fun setUp() {
        viewModel = TMDBViewModel(AppDefinition())
        IdlingRegistry.getInstance().register(IdlingResourceInstance.currentResource)
    }

    @get:Rule
    var activityRule = activityScenarioRule<MainActivity>()

    @After
    fun tearDown() {
        IdlingRegistry.getInstance().unregister(IdlingResourceInstance.currentResource)
        viewModel = null
    }

    @Test
    fun checkMenu() {

        // checkMenu() is a series of tests that deals with menu items on BottomNavigationView

        // Check if there is a view displayed with id of MISearch
        onView(withId(R.id.MISearch)).check(matches(isDisplayed()))

        // Check if there is a view displayed with id of MIFavoritesM
        onView(withId(R.id.MIFavoritesM)).check(matches(isDisplayed()))

        // Check if there is a view displayed with id of MIFavoritesT
        onView(withId(R.id.MIFavoritesT)).check(matches(isDisplayed()))
    }

    @Test
    fun checkSearchPageFragment() {

        // Check if there is a view displayed with id of MISearch and then try to perform click()
        onView(withId(R.id.MISearch)).check(matches(isDisplayed())).perform(click())

        // Check if there is a view displayed with id of MISearchToggle and then try to perform click()
        onView(withId(R.id.MISearchToggle)).check(matches(isDisplayed())).perform(click())

        // Check if there are any displayed EditText view and then try to clearText(), and typeText(), and press KeyEvent.KEYCODE_ENTER
        onView(allOf(isDisplayed(), isAssignableFrom(EditText::class.java))).perform(clearText(), typeText("the"), pressKey(KeyEvent.KEYCODE_ENTER))

        // Close the soft keyboard down
        closeSoftKeyboard()

        // Check if there is a view displayed with id of VPContentList
        onView(withId(R.id.VPContentList)).check(matches(isDisplayed()))

        // Check if there is a view displayed with text of "Movies" and then try to perform click()
        onView(withText("Movies")).check(matches(isDisplayed())).perform(click())

        // Check if there is a view displayed with id of RVItem and then try to perform scrollToPosition()
        onView(allOf(isDisplayed(), withId(R.id.RVItem))).check(matches(isDisplayed())).perform(RecyclerViewActions.scrollToPosition<RecyclerView.ViewHolder>(19))

        // Check if there is a view displayed with text of "TV Shows" and then try to perform click()
        onView(withText("TV Shows")).check(matches(isDisplayed())).perform(click())

        // Check if there is a view displayed with id of RVItem and then try to perform scrollToPosition()
        onView(allOf(isDisplayed(), withId(R.id.RVItem))).check(matches(isDisplayed())).perform(RecyclerViewActions.scrollToPosition<RecyclerView.ViewHolder>(19))
    }

    @Test
    fun checkFavoriteMoviesPageFragment() {

        // Check if there is a view displayed with id of MIFavoritesM and then try to perform click()
        onView(withId(R.id.MIFavoritesM)).check(matches(isDisplayed())).perform(click())

        // Check if there is a view hidden with id of RVItem (note this must be invisible if there are no favorites in the list for movies)
        onView(withId(R.id.RVItem)).check(matches(not(isDisplayed())))

        // Check if there is a view hidden with id of IVStateIcon (note this must be visible if there are no favorites in the list for movies)
        onView(withId(R.id.IVStateIcon)).check(matches(isDisplayed()))

        // Check if there is a view hidden with id of TVStateText (note this must be visible if there are no favorites in the list for movies)
        onView(withId(R.id.TVStateText)).check(matches(isDisplayed()))
    }

    @Test
    fun checkFavoriteTVShowsPageFragment() {

        // Check if there is a view displayed with id of MIFavoritesT and then try to perform click()
        onView(withId(R.id.MIFavoritesT)).check(matches(isDisplayed())).perform(click())

        // Check if there is a view hidden with id of RVItem (note this must be invisible if there are no favorites in the list for tv shows)
        onView(withId(R.id.RVItem)).check(matches(not(isDisplayed())))

        // Check if there is a view hidden with id of IVStateIcon (note this must be visible if there are no favorites in the list for tv shows)
        onView(withId(R.id.IVStateIcon)).check(matches(isDisplayed()))

        // Check if there is a view hidden with id of TVStateText (note this must be visible if there are no favorites in the list for tv shows)
        onView(withId(R.id.TVStateText)).check(matches(isDisplayed()))
    }
}