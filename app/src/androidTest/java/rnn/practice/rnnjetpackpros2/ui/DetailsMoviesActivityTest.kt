package rnn.practice.rnnjetpackpros2.ui

import android.view.KeyEvent
import android.widget.EditText
import androidx.recyclerview.widget.RecyclerView
import androidx.test.core.app.launchActivity
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.IdlingRegistry
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.rules.activityScenarioRule
import org.hamcrest.CoreMatchers
import org.hamcrest.CoreMatchers.allOf
import org.hamcrest.CoreMatchers.not
import org.junit.After
import org.junit.Before

import org.junit.Assert.*
import org.junit.Rule
import org.junit.Test
import rnn.practice.rnnjetpackpros2.R
import rnn.practice.rnnjetpackpros2.application.AppDefinition
import rnn.practice.rnnjetpackpros2.utils.IdlingResourceInstance
import rnn.practice.rnnjetpackpros2.viewmodels.TMDBViewModel

class DetailsMoviesActivityTest {

    private var viewModel: TMDBViewModel? = null

    private val detailsActivityViews = arrayListOf(R.id.IVPoster, R.id.TVTitle, R.id.TVYear, R.id.TVOverview, R.id.TVGenre, R.id.TVType, R.id.TVContentRating, R.id.TVDuration, R.id.BSource)

    @Before
    fun setUp() {
        viewModel = TMDBViewModel(AppDefinition())
        IdlingRegistry.getInstance().register(IdlingResourceInstance.currentResource)
    }

    @get:Rule
    var activityRule = activityScenarioRule<MainActivity>()

    @After
    fun tearDown() {
        IdlingRegistry.getInstance().unregister(IdlingResourceInstance.currentResource)
        viewModel = null
    }

    @Test
    fun testItems() {

        // Check if there is a view displayed with id of MISearch and then try to perform click()
        onView(withId(R.id.MISearch)).check(matches(isDisplayed())).perform(click())

        // Check if there is a view displayed with id of MISearchToggle and then try to perform click()
        onView(withId(R.id.MISearchToggle)).check(matches(isDisplayed())).perform(click())

        // Check if there are any displayed EditText view and then try to clearText(), and typeText(), and press KeyEvent.KEYCODE_ENTER
        onView(allOf(isDisplayed(), isAssignableFrom(EditText::class.java))).perform(clearText(), typeText("the"), pressKey(KeyEvent.KEYCODE_ENTER))

        // Close the soft keyboard down
        closeSoftKeyboard()

        // Check if there is a view displayed with id of VPContentList
        onView(withId(R.id.VPContentList)).check(matches(isDisplayed()))

        // Execute testSingleItem() for item position 19, 3, (19 / 2  + (19 / 4)), (19 / 3) without testing add-to-Favorites feature
        testSingleItem(false, 19, 3, (19 / 2  + (19 / 4)), (19 / 3))
    }

    @Test
    fun testFavorites() {

        // Check if there is a view displayed with id of MISearch and then try to perform click() to open FavoriteMoviesPageFragment
        onView(withId(R.id.MISearch)).check(matches(isDisplayed())).perform(click())

        // Check if there is a view displayed with id of MISearchToggle and then try to perform click()
        onView(withId(R.id.MISearchToggle)).check(matches(isDisplayed())).perform(click())

        // Check if there are any displayed EditText view and then try to clearText(), and typeText(), and press KeyEvent.KEYCODE_ENTER
        onView(allOf(isDisplayed(), isAssignableFrom(EditText::class.java))).perform(clearText(), typeText("the"), pressKey(KeyEvent.KEYCODE_ENTER))

        // Close the soft keyboard down
        closeSoftKeyboard()

        // Check if there is a view displayed with id of VPContentList
        onView(withId(R.id.VPContentList)).check(matches(isDisplayed()))

        // Execute testSingleItem() for item position 19, 3, (19 / 2  + (19 / 4)), (19 / 3) and test add-to-Favorites feature
        testSingleItem(true, 19, 3, (19 / 2  + (19 / 4)), (19 / 3))

        // Execute testSingleItemFavorite() for item position 1, 2, 3, 4 and test remove-from-Favorites feature
        testSingleItemFavorite( 4)

        // Check if there is a view displayed with id of MISearch and then try to perform click() to get back to the FavoriteMoviesPageFragment
        onView(withId(R.id.MIFavoritesM)).check(matches(isDisplayed())).perform(click())

        // Now we have removed every item from the favorite list, Check if there is a view hidden with id of RVItem (note this must be invisible if there are no favorites in the list for movies)
        onView(withId(R.id.RVItem)).check(matches(not(isDisplayed())))

        // Check if there is a view hidden with id of IVStateIcon (note this must be visible if there are no favorites in the list for movies)
        onView(withId(R.id.IVStateIcon)).check(matches(isDisplayed()))

        // Check if there is a view hidden with id of TVStateText (note this must be visible if there are no favorites in the list for movies)
        onView(withId(R.id.TVStateText)).check(matches(isDisplayed()))
    }

    private fun testSingleItem(withFavorite: Boolean = false, vararg positions: Int) {

        for (currentIndex in positions) {

            // Check if there is a view displayed with text of "Movies" and then try to perform click()
            onView(withText("Movies")).check(matches(isDisplayed())).perform(click())

            // Check if there is a view displayed with id of RVItem and then try to perform scrollToPosition()
            onView(allOf(isDisplayed(), withId(R.id.RVItem))).check(matches(isDisplayed())).perform(RecyclerViewActions.scrollToPosition<RecyclerView.ViewHolder>(currentIndex)).perform(click())

            // Check if there is a view displayed with id of FABFavorite and then click the FABFavorite if withFavorite parameter is true to add this movie to the favorite list
            if (withFavorite) {
                onView(withId(R.id.FABFavorite)).check(matches(isDisplayed())).perform(click())
            }

            // for every single view id on detailsActivityViews array, check whether it is displayed and perform click if it has the id of BSource
            detailsActivityViews.forEach {
                if (it == detailsActivityViews.last()) {
                    onView(withId(it)).perform(scrollTo()).check(matches(isDisplayed())).perform(click())
                } else {
                    onView(withId(it)).perform(scrollTo()).check(matches(isDisplayed()))
                }
            }

            // relaunch MainActivity
            launchActivity<MainActivity>()
        }
    }

    private fun testSingleItemFavorite(itemCount: Int) {

        repeat(itemCount) {

            // Check if there is a view displayed with id of MISearch and then try to perform click()
            onView(withId(R.id.MIFavoritesM)).check(matches(isDisplayed())).perform(click())

            // Check if there is a view displayed with id of RVItem (note this must be invisible if there are no favorites in the list for movie and the same rule apply the other way around)
            onView(withId(R.id.RVItem)).check(matches(isDisplayed())).perform(RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(0, click()))

            // for every single view id on detailsActivityViews array, check whether it is displayed
            detailsActivityViews.forEach {
                onView(withId(it)).perform(scrollTo()).check(matches(isDisplayed()))
            }

            // Check if there is a view displayed with id of FABFavorite and then click the FABFavorite if withFavorite parameter is true to remove this movie from the favorites list
            onView(withId(R.id.FABFavorite)).check(matches(isDisplayed())).perform(click())

            // relaunch MainActivity
            launchActivity<MainActivity>()
        }
    }
}