package rnn.practice.rnnjetpackpros2.routers

import com.github.kittinunf.fuel.core.HeaderValues
import com.github.kittinunf.fuel.core.Headers
import com.github.kittinunf.fuel.core.Method
import com.github.kittinunf.fuel.core.Parameters
import com.github.kittinunf.fuel.util.FuelRouting
import rnn.practice.rnnjetpackpros2.BuildConfig

sealed class TMDBImageRouter : FuelRouting {
    override val basePath: String
        get() {
            return "https://image.tmdb.org/t/p"
        }

    class ImageOriginal(val sizeString: String) : TMDBImageRouter()
    class ImageMedium(val sizeString: String) : TMDBImageRouter()
    class ImageNormal(val sizeString: String) : TMDBImageRouter()
    class ImageSmall(val sizeString: String) : TMDBImageRouter()
    class ImageTiny(val sizeString: String) : TMDBImageRouter()

    override val method: Method
        get() {
            return Method.GET
        }

    override val params: Parameters?
        get() {
            return listOf("api_key" to BuildConfig.RNN_TMDB_TOKEN)
        }

    override val body: String?
        get() {
            return null
        }

    override val path: String
        get() {
            return when(this) {
                is ImageOriginal -> "/original${this.sizeString}"
                is ImageMedium -> "/500${this.sizeString}"
                is ImageNormal -> "/342${this.sizeString}"
                is ImageSmall -> "/154${this.sizeString}"
                is ImageTiny -> "/92${this.sizeString}"
            }
        }

    override val headers: Map<String, HeaderValues>?
        get() {
            return Headers()
                    .set(Headers.CONTENT_TYPE, "application/json")
        }

    override val bytes: ByteArray?
        get() {
            return null
        }
}
