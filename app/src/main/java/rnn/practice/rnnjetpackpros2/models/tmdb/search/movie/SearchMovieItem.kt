package rnn.practice.rnnjetpackpros2.models.tmdb.search.movie

import com.google.gson.annotations.SerializedName

data class SearchMovieItem(
    @SerializedName("id")
    var intId: Int = 0,

    @SerializedName("title")
    var stringTitle: String? = null,

    @SerializedName("release_date")
    var stringReleaseDate: String? = "yyyy-mm-dd",

    @SerializedName("overview")
    var stringOverview: String? = null,

    @SerializedName("poster_path")
    var stringPoster: String? = null
)
