package rnn.practice.rnnjetpackpros2.models.tmdb.movie.releasedates

import com.google.gson.annotations.SerializedName
import rnn.practice.rnnjetpackpros2.models.tmdb.movie.releasedates.certifications.MovieCertification

data class MovieReleaseDate(
    @SerializedName("iso_3166_1")
    var stringCountry: String? = null,

    @SerializedName("release_dates")
    var arrayCertifications: ArrayList<MovieCertification>
)
