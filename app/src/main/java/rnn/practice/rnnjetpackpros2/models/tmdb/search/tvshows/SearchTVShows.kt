package rnn.practice.rnnjetpackpros2.models.tmdb.search.tvshows

import com.google.gson.annotations.SerializedName

data class SearchTVShows(
    @SerializedName("results")
    var arrayResults: ArrayList<SearchTVShowsItem>,

    @SerializedName("page")
    var intPage: Int = 0,

    @SerializedName("total_results")
    var intTotal_pages: Int = 0,

    @SerializedName("total_pages")
    var intTotal_results: Int = 0
)
