package rnn.practice.rnnjetpackpros2.models.tmdb.search.movie

import com.google.gson.annotations.SerializedName


data class SearchMovie(
    @SerializedName("results")
    var arrayResults: ArrayList<SearchMovieItem>,

    @SerializedName("page")
    var intPage: Int = 0,

    @SerializedName("total_results")
    var intTotal_pages: Int = 0,

    @SerializedName("total_pages")
    var intTotal_results: Int = 0
)
