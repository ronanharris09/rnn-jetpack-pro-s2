package rnn.practice.rnnjetpackpros2.models.tmdb.genres

import com.google.gson.annotations.SerializedName

data class ShowGenre(
    @SerializedName("name")
    var stringGenre: String? = null
)
