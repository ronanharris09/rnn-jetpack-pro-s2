package rnn.practice.rnnjetpackpros2.models.roomdb

import android.provider.BaseColumns
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = FavoriteRemoteKey.TABLE_NAME)
data class FavoriteRemoteKey (

    @PrimaryKey
    val showId: Int,

    val previousKeyId: Int?,

    val nextKeyId: Int?
) {
    companion object {
        const val TABLE_NAME = "table_favorite_keys"
    }
}