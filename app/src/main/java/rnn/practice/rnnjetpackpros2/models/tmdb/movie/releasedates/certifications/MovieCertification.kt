package rnn.practice.rnnjetpackpros2.models.tmdb.movie.releasedates.certifications

import com.google.gson.annotations.SerializedName

data class MovieCertification(
    @SerializedName("certification")
    var stringCertification: String? = null,
)
