package rnn.practice.rnnjetpackpros2.utils

enum class RNNState {
    INIT, LOAD, DONE, FAIL
}