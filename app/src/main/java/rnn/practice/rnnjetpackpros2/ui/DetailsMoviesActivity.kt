package rnn.practice.rnnjetpackpros2.ui

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.google.android.material.snackbar.Snackbar
import org.kodein.di.DI
import org.kodein.di.DIAware
import org.kodein.di.android.closestDI
import org.kodein.di.instance
import rnn.practice.rnnjetpackpros2.R
import rnn.practice.rnnjetpackpros2.databinding.ActivityDetailsMoviesBinding
import rnn.practice.rnnjetpackpros2.models.roomdb.FavoriteTMDBShow
import rnn.practice.rnnjetpackpros2.models.tmdb.genres.ShowGenre
import rnn.practice.rnnjetpackpros2.viewmodels.TMDBViewModel

class DetailsMoviesActivity : AppCompatActivity(), DIAware {

    companion object {
        private const val EXTERNAL_LINK = "https://www.themoviedb.org/movies/"
        private const val TMDB_BASE_IMAGE_PATH = "https://image.tmdb.org/t/p/w300"
    }

    override val di: DI by closestDI()

    private val viewModel: TMDBViewModel by instance()

    private lateinit var binding: ActivityDetailsMoviesBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDetailsMoviesBinding.inflate(layoutInflater)
        setContentView(binding.root)

        viewModel.getMovie().observe(this, {

            viewModel.findFavorite(it.intId)

            binding.apply {
                TVTitle.text = it.stringTitle
                TVYear.text = it.stringReleaseDate
                TVGenre.text = aggregateGenreItems(it.arrayGenres)
                TVContentRating.text = if (!it.objectReleaseDatesResults.arrayReleaseDates.filter { it.stringCountry == "US" }.isNullOrEmpty()) it.objectReleaseDatesResults.arrayReleaseDates.filter { it.stringCountry == "US" }[0].arrayCertifications[0].stringCertification ?: "-" else "-"
                TVDuration.text = if (it.duration == 0) resources.getString(R.string.TVDuration, ((it.duration - (it.duration % 60)) / 60), (it.duration % 60)) else "-"
                TVType.text = resources.getQuantityText(R.plurals.TVType, 0)
                TVOverview.text = it.stringOverview
                BSource.setOnClickListener { _ ->
                    startActivity(
                        Intent(
                            Intent.ACTION_VIEW, Uri.parse("$EXTERNAL_LINK${it.intId}")
                        )
                    )
                }
                viewModel.getFavorite().observe(this@DetailsMoviesActivity, { itemIndex ->
                    FABFavorite.apply {
                        isSelected = (itemIndex != null)
                        setOnClickListener { _ ->
                            if (itemIndex == null) {
                                viewModel.addFavorite(
                                    FavoriteTMDBShow(
                                        stringTitle =  it.stringTitle,
                                        stringOverview = it.stringOverview,
                                        stringDate = it.stringReleaseDate,
                                        stringPoster =  it.stringPoster,
                                        isMovie =  true,
                                        intId = it.intId
                                    )
                                )
                                Snackbar.make(
                                    root,
                                    resources.getString(R.string.SnackbarAddFavorite),
                                    Snackbar.LENGTH_LONG
                                ).show()
                            } else {
                                viewModel.removeFavorite(
                                    FavoriteTMDBShow(
                                        stringTitle =  it.stringTitle,
                                        stringOverview = it.stringOverview,
                                        stringDate = it.stringReleaseDate,
                                        stringPoster =  it.stringPoster,
                                        isMovie =  true,
                                        intId = it.intId,
                                        keyIndex = itemIndex
                                    )
                                )
                                Snackbar.make(
                                    root,
                                    resources.getString(R.string.SnackbarRemoveFavorite),
                                    Snackbar.LENGTH_LONG
                                ).show()
                            }

                            viewModel.findFavorite(it.intId)
                        }
                    }
                })
                Glide.with(this.root)
                    .load("$TMDB_BASE_IMAGE_PATH${it.stringPoster}")
                    .placeholder(R.drawable.ic_baseline_image_search_24)
                    .error(R.drawable.ic_baseline_broken_image_24)
                    .into(IVPoster)
            }
        })
    }

    override fun onDestroy() {
        super.onDestroy()
        viewModel.getMovie().removeObservers(this)
    }

    private fun aggregateGenreItems(arrayList: ArrayList<ShowGenre>) : String {
        var string = ""

        if (!arrayList.isNullOrEmpty()) {
            for (item in arrayList) {
                string += if (item != arrayList.last()) "${item.stringGenre}, " else "${item.stringGenre}."
            }
        } else {
            string = "-"
        }

        return string
    }
}