package rnn.practice.rnnjetpackpros2.ui

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.google.android.material.snackbar.Snackbar
import org.kodein.di.DI
import org.kodein.di.DIAware
import org.kodein.di.android.closestDI
import org.kodein.di.instance
import rnn.practice.rnnjetpackpros2.R
import rnn.practice.rnnjetpackpros2.databinding.ActivityDetailsTVShowBinding
import rnn.practice.rnnjetpackpros2.models.roomdb.FavoriteTMDBShow
import rnn.practice.rnnjetpackpros2.models.tmdb.genres.ShowGenre
import rnn.practice.rnnjetpackpros2.viewmodels.TMDBViewModel

class DetailsTVShowActivity : AppCompatActivity(), DIAware {

    companion object {
        private const val EXTERNAL_LINK = "https://www.themoviedb.org/movies/"
        private const val TMDB_BASE_IMAGE_PATH = "https://image.tmdb.org/t/p/w300"
    }

    override val di: DI by closestDI()

    private val viewModel: TMDBViewModel by instance()

    private lateinit var binding: ActivityDetailsTVShowBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDetailsTVShowBinding.inflate(layoutInflater)
        setContentView(binding.root)

        viewModel.getTVShow().observe(this, {

            viewModel.findFavorite(it.intId)

            binding.apply {
                TVTitle.text = it.stringTitle
                TVYear.text = it.stringFirstAirDate
                TVGenre.text = aggregateGenreItems(it.arrayGenres)
                TVContentRating.text = if (!it.objectTVShowsContentRatingsResult.arrayContentRating.filter { it.stringCountry == "US" }.isNullOrEmpty()) it.objectTVShowsContentRatingsResult.arrayContentRating.filter { it.stringCountry == "US" }[0].stringRating else "-"
                TVDuration.text = if (it.episodeCount != 0 && !it.episodeDuration.isNullOrEmpty()) resources.getString(R.string.TVDuration, (((it.episodeDuration?.get(0)?.times(it.episodeCount))?.minus((((it.episodeDuration?.get(0)?.times(it.episodeCount))?.rem(60))?.div(60)) ?: 0))), ((it.episodeDuration?.get(0)?.times(it.episodeCount))?.rem(60))) else "-"
                TVType.text = resources.getQuantityText(R.plurals.TVType, 1)
                TVOverview.text = it.stringOverview
                BSource.setOnClickListener { _ ->
                    startActivity(
                        Intent(
                                Intent.ACTION_VIEW, Uri.parse("$EXTERNAL_LINK${it.intId}")
                        )
                    )
                }
                viewModel.getFavorite().observe(this@DetailsTVShowActivity, { itemIndex ->
                    FABFavorite.apply {
                        isSelected = (itemIndex != null)
                        setOnClickListener { _ ->
                            if (itemIndex == null) {
                                viewModel.addFavorite(
                                    FavoriteTMDBShow(
                                        stringTitle =  it.stringTitle,
                                        stringOverview = it.stringOverview,
                                        stringDate = it.stringFirstAirDate,
                                        stringPoster =  it.stringPoster,
                                        isMovie =  false,
                                        intId = it.intId
                                    )
                                )
                                Snackbar.make(
                                    root,
                                    resources.getString(R.string.SnackbarAddFavorite),
                                    Snackbar.LENGTH_LONG
                                ).show()
                            } else {
                                viewModel.removeFavorite(
                                    FavoriteTMDBShow(
                                        stringTitle =  it.stringTitle,
                                        stringOverview = it.stringOverview,
                                        stringDate = it.stringFirstAirDate,
                                        stringPoster =  it.stringPoster,
                                        isMovie =  false,
                                        intId = it.intId,
                                        keyIndex = itemIndex
                                    )
                                )
                                Snackbar.make(
                                    root,
                                    resources.getString(R.string.SnackbarRemoveFavorite),
                                    Snackbar.LENGTH_LONG
                                ).show()
                            }

                            viewModel.findFavorite(it.intId)
                        }
                    }
                })
                Glide.with(this.root)
                    .load("$TMDB_BASE_IMAGE_PATH${it.stringPoster}")
                    .placeholder(R.drawable.ic_baseline_image_search_24)
                    .error(R.drawable.ic_baseline_broken_image_24)
                    .into(IVPoster)
            }
        })
    }

    override fun onDestroy() {
        super.onDestroy()
        viewModel.getMovie().removeObservers(this)
    }

    private fun aggregateGenreItems(arrayList: ArrayList<ShowGenre>) : String {
        var string = ""

        if (!arrayList.isNullOrEmpty()) {
            for (item in arrayList) {
                string += if (item != arrayList.last()) "${item.stringGenre}, " else "${item.stringGenre}."
            }
        } else {
            string = "-"
        }

        return string
    }
}