package rnn.practice.rnnjetpackpros2.fragments

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import org.kodein.di.DI
import org.kodein.di.DIAware
import org.kodein.di.android.x.closestDI
import org.kodein.di.instance
import rnn.practice.rnnjetpackpros2.adapters.SearchTVShowsResultAdapter
import rnn.practice.rnnjetpackpros2.databinding.FragmentSearchTVShowsBinding
import rnn.practice.rnnjetpackpros2.ui.DetailsTVShowActivity
import rnn.practice.rnnjetpackpros2.viewmodels.TMDBViewModel

class SearchTVShowsFragment : Fragment(), DIAware {

    override val di: DI by closestDI()

    private val viewModel: TMDBViewModel by instance()

    private lateinit var binding: FragmentSearchTVShowsBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentSearchTVShowsBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.getTVShows().observe(this, {
            if (!it.arrayResults.isNullOrEmpty()) {
                binding.apply {
                    RVItem.apply {
                        visibility = View.VISIBLE
                        adapter = SearchTVShowsResultAdapter(it.arrayResults) { item ->
                            viewModel.queryTVShow(item.intId.toString())
                            startActivity(Intent(context, DetailsTVShowActivity::class.java))
                        }
                        layoutManager = LinearLayoutManager(context)
                        contentDescription = "Search TV Shows Result"
                        setHasFixedSize(true)
                    }

                    LLStateContainer.visibility = View.INVISIBLE
                    IVStateIcon.visibility = View.INVISIBLE
                    TVStateText.visibility = View.INVISIBLE
                }
            } else {
                binding.apply {
                    RVItem.visibility = View.INVISIBLE
                    LLStateContainer.visibility = View.VISIBLE
                    IVStateIcon.visibility = View.VISIBLE
                    TVStateText.visibility = View.VISIBLE
                }
            }
        })
    }
}