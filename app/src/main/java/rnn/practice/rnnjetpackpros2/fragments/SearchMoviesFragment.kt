package rnn.practice.rnnjetpackpros2.fragments

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import org.kodein.di.DI
import org.kodein.di.DIAware
import org.kodein.di.android.x.closestDI
import org.kodein.di.instance
import rnn.practice.rnnjetpackpros2.adapters.SearchMoviesResultAdapter
import rnn.practice.rnnjetpackpros2.databinding.FragmentSearchMoviesBinding
import rnn.practice.rnnjetpackpros2.ui.DetailsMoviesActivity
import rnn.practice.rnnjetpackpros2.viewmodels.TMDBViewModel

class SearchMoviesFragment : Fragment(), DIAware {

    override val di: DI by closestDI()

    private val viewModel: TMDBViewModel by instance()

    private lateinit var binding: FragmentSearchMoviesBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentSearchMoviesBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.getMovies().observe(this, {
            if (!it.arrayResults.isNullOrEmpty()) {
                binding.apply {
                    RVItem.apply {
                        visibility = View.VISIBLE
                        adapter = SearchMoviesResultAdapter(it.arrayResults) { item ->
                            viewModel.queryMovie(item.intId.toString())
                            startActivity(Intent(context, DetailsMoviesActivity::class.java))
                        }
                        layoutManager = LinearLayoutManager(context)
                        contentDescription = "Search Movies Result"
                        setHasFixedSize(true)
                    }

                    LLStateContainer.visibility = View.INVISIBLE
                    IVStateIcon.visibility = View.INVISIBLE
                    TVStateText.visibility = View.INVISIBLE
                }
            } else {
                binding.apply {
                    RVItem.visibility = View.INVISIBLE
                    LLStateContainer.visibility = View.VISIBLE
                    IVStateIcon.visibility = View.VISIBLE
                    TVStateText.visibility = View.VISIBLE
                }
            }
        })
    }
}