package rnn.practice.rnnjetpackpros2.fragments

import android.content.Context
import android.os.Bundle
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import com.bumptech.glide.Glide
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.kodein.di.DI
import org.kodein.di.DIAware
import org.kodein.di.android.x.closestDI
import org.kodein.di.instance
import rnn.practice.rnnjetpackpros2.R
import rnn.practice.rnnjetpackpros2.adapters.SearchShowsViewPagerAdapter
import rnn.practice.rnnjetpackpros2.databinding.FragmentSearchPageBinding
import rnn.practice.rnnjetpackpros2.interfaces.IState
import rnn.practice.rnnjetpackpros2.utils.IdlingResourceInstance
import rnn.practice.rnnjetpackpros2.utils.RNNState
import rnn.practice.rnnjetpackpros2.viewmodels.TMDBViewModel

class SearchPageFragment : Fragment(), DIAware, IState {

    override val di: DI by closestDI()

    private val viewModel: TMDBViewModel by instance()

    private lateinit var binding: FragmentSearchPageBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setHasOptionsMenu(true)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = FragmentSearchPageBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (viewModel.getState().value == null) {
            viewModel.setState(RNNState.INIT)
        }

        binding.apply {
            TFragment.apply {
                title = "Find Shows on TMDB"
                inflateMenu(R.menu.toolbar_search_fragment_menu)
                menu.getItem(0).setOnMenuItemClickListener {
                    searchMenuToggleListener(it)
                }
            }
            VPContentList.adapter = SearchShowsViewPagerAdapter(context!!, childFragmentManager)
            TLShowTypes.setupWithViewPager(VPContentList)
        }

        triggerState(viewModel.getState().value)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
    }

    override fun onDestroy() {
        super.onDestroy()
        viewModel.getState().removeObservers(this)
    }

    override fun toggleState(state: RNNState?, className: String) {
        super.toggleState(state, SearchPageFragment::class.java.simpleName)

        lifecycleScope.launch(Dispatchers.Main) {
            viewModel.getState().observe(this@SearchPageFragment, {
                when(it) {
                    RNNState.INIT -> {

                    }
                    RNNState.LOAD -> {
                        IdlingResourceInstance.incrementCounter()
                        binding.apply {
                            TLShowTypes.visibility = View.INVISIBLE
                            VPContentList.visibility = View.INVISIBLE
                            LLStateContainer.visibility = View.VISIBLE
                            TVStateText.apply {
                                text = getString(R.string.String_State_LOAD)
                                visibility = View.VISIBLE
                            }
                            Glide.with(this.root)
                                .load(R.drawable.ic_baseline_local_florist_24)
                                .into(IVStateIcon)
                            IVStateIcon.visibility = View.VISIBLE
                        }
                    }
                    RNNState.DONE -> {
                        IdlingResourceInstance.decrementCounter()
                        binding.apply {
                            TVStateText.visibility = View.INVISIBLE
                            IVStateIcon.visibility = View.INVISIBLE
                            LLStateContainer.visibility = View.INVISIBLE
                            TLShowTypes.visibility = View.VISIBLE
                            VPContentList.visibility = View.VISIBLE
                        }
                    }
                    RNNState.FAIL -> {
                        IdlingResourceInstance.decrementCounter()
                        binding.apply {
                            TLShowTypes.visibility = View.INVISIBLE
                            VPContentList.visibility = View.INVISIBLE
                            LLStateContainer.visibility = View.VISIBLE
                            TVStateText.apply {
                                text = getString(R.string.String_State_FAIL)
                                visibility = View.VISIBLE
                            }
                            Glide.with(this.root)
                                    .load(R.drawable.ic_baseline_error_outline_24)
                                    .into(IVStateIcon)
                            IVStateIcon.visibility = View.VISIBLE
                        }
                    }
                    null -> {

                    }
                }
            })
        }
    }

    private fun triggerState(state: RNNState?) {
        toggleState(state, SearchPageFragment::class.java.simpleName)
    }

    private fun searchMenuToggleListener(menuItem: MenuItem) : Boolean {

        (menuItem.actionView as androidx.appcompat.widget.SearchView).apply {
            contentDescription = "zzz"
            queryHint = "Type a search keyword here..."

            setOnQueryTextListener(object : SearchView.OnQueryTextListener, androidx.appcompat.widget.SearchView.OnQueryTextListener {
                override fun onQueryTextSubmit(query: String?): Boolean {

                    viewModel.querySearch(query.toString()) {
                        triggerState(viewModel.getState().value)
                    }

                    closeSoftKeyboard()

                    return true
                }

                override fun onQueryTextChange(newText: String?): Boolean {
                    return false
                }
            })
        }

        return true
    }

    private fun closeSoftKeyboard() = (activity?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager).hideSoftInputFromWindow(activity?.currentFocus?.windowToken, 0)
}