package rnn.practice.rnnjetpackpros2.application

import android.app.Application
import androidx.paging.ExperimentalPagingApi
import org.kodein.di.*
import org.kodein.di.android.x.androidXModule
import rnn.practice.rnnjetpackpros2.database.FavoriteTMDBShowDB
import rnn.practice.rnnjetpackpros2.repository.RoomDBRepository
import rnn.practice.rnnjetpackpros2.repository.TMDBRepository
import rnn.practice.rnnjetpackpros2.utils.FavoriteMediator
import rnn.practice.rnnjetpackpros2.utils.RNNFuelInstance
import rnn.practice.rnnjetpackpros2.viewmodels.TMDBViewModel

class AppDefinition : Application(), DIAware {
    @ExperimentalPagingApi
    override val di: DI by DI.lazy {
        import(androidXModule(this@AppDefinition))

        bind { singleton { RNNFuelInstance.init() } }
        bind { singleton { FavoriteTMDBShowDB.initDatabase(instance()) } }
        bind { singleton { RoomDBRepository(instance(), instance()) } }
        bind { singleton { TMDBRepository(instance()) } }
        bind { singleton { TMDBViewModel(instance()) } }
    }
}