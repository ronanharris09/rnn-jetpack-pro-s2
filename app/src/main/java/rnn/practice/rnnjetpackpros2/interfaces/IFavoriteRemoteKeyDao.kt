package rnn.practice.rnnjetpackpros2.interfaces

import androidx.room.*
import rnn.practice.rnnjetpackpros2.models.roomdb.FavoriteRemoteKey

@Dao
interface IFavoriteRemoteKeyDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun pagingInsertKeys(collection: List<FavoriteRemoteKey>)

    @Query("SELECT * FROM table_favorite_keys WHERE showId = :id")
    suspend fun pagingGetKey(id: Int) : FavoriteRemoteKey?

    @Query("DELETE FROM table_favorite_keys")
    suspend fun pagingResetKeys()
}