package rnn.practice.rnnjetpackpros2.interfaces

import com.github.kittinunf.fuel.core.FuelError
import com.github.kittinunf.result.Result

interface IServiceTMDB {

    suspend fun searchMovies(titleString: String) : Result<String, FuelError>

    suspend fun searchTVShows(titleString: String) : Result<String, FuelError>

    suspend fun detailsMovie(idString: String) : Result<String, FuelError>

    suspend fun detailsTVShow(idString: String) : Result<String, FuelError>
}