package rnn.practice.rnnjetpackpros2.interfaces

import androidx.paging.PagingSource
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import rnn.practice.rnnjetpackpros2.models.roomdb.FavoriteTMDBShow

@Dao
interface IPagingFavoriteShowDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun pagingInsertShows(collection: List<FavoriteTMDBShow>)

    @Query("SELECT * FROM table_favorite")
    fun pagingGetShows() : PagingSource<Int, FavoriteTMDBShow>

    @Query("SELECT * FROM table_favorite")
    fun getShows() : List<FavoriteTMDBShow>?

    @Query("DELETE FROM table_favorite")
    suspend fun pagingResetShows()
}