package rnn.practice.rnnjetpackpros2.adapters

import android.content.Context
import androidx.annotation.StringRes
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import rnn.practice.rnnjetpackpros2.R
import rnn.practice.rnnjetpackpros2.fragments.SearchMoviesFragment
import rnn.practice.rnnjetpackpros2.fragments.SearchTVShowsFragment

class SearchShowsViewPagerAdapter(private val context: Context, fragmentManager: FragmentManager) : FragmentPagerAdapter(fragmentManager, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    companion object {
        @StringRes
        private val TAB_TITLES = intArrayOf(
            R.string.String_Movies,
            R.string.String_TVShows
        )
    }

    override fun getCount() = 2

    override fun getItem(position: Int): Fragment {
        return when(position) {
            0 -> SearchMoviesFragment()
            1 -> SearchTVShowsFragment()
            else -> Fragment()
        }
    }

    override fun getPageTitle(position: Int): CharSequence = context.getString(TAB_TITLES[position])
}